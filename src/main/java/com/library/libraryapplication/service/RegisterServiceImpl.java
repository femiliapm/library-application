package com.library.libraryapplication.service;

import com.library.libraryapplication.assembler.RegisterAssembler;
import com.library.libraryapplication.model.dto.RegisterDto;
import com.library.libraryapplication.model.entity.DetailBiodata;
import com.library.libraryapplication.repository.BiodataRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private BiodataRepository repository;
    @Autowired
    private RegisterAssembler assembler;

    @Override
    public RegisterDto insert(RegisterDto dto) {
        // TODO Auto-generated method stub
        DetailBiodata detailBiodata = assembler.fromDto(dto);
        repository.save(detailBiodata);
        return assembler.fromEntity(detailBiodata);
    }

}
