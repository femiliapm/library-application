package com.library.libraryapplication.service;

import com.library.libraryapplication.model.dto.RegisterDto;

public interface RegisterService {
   RegisterDto insert(RegisterDto dto);
}
