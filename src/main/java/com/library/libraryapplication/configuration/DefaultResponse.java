package com.library.libraryapplication.configuration;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class DefaultResponse<T> {
  private Integer status;
  private String message;
  private T data;

  public DefaultResponse(Integer status, String message, T data) {
    this.status = status;
    this.message = message;
    this.data = data;
  }

  public static <T> DefaultResponse<T> ok(T data) {
    return new DefaultResponse(HttpStatus.OK.value(), "Success!", data);
  }

  public static <T> DefaultResponse<T> ok(T data, String message) {
    return new DefaultResponse(HttpStatus.OK.value(), message, data);
  }

  public static <T> DefaultResponse<T> error(String message) {
    return new DefaultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
  }
}
