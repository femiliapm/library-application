package com.library.libraryapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryApplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplicationApplication.class, args);
	}

}
