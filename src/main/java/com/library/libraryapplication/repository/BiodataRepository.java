package com.library.libraryapplication.repository;

import com.library.libraryapplication.model.entity.DetailBiodata;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BiodataRepository extends JpaRepository<DetailBiodata, Integer>{

}
