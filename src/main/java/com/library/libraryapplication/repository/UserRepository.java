package com.library.libraryapplication.repository;

import com.library.libraryapplication.model.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    
}
