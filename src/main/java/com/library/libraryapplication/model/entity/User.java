package com.library.libraryapplication.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tabel_user")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_user", unique = true, updatable = false)
  private Integer idUser;
  @Column(name = "username", unique = true, updatable = false, nullable = false)
  private String username;
  @Column(name = "password", unique = true, nullable = false)
  private String password;
}
