package com.library.libraryapplication.model.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tabel_detail_biodata")
public class DetailBiodata {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_detail", unique = true, updatable = false)
  private Integer idDetail;
  @Column(name = "nama_lengkap")
  private String namaLengkap;
  @Column(name = "no_identitas", unique = true, nullable = false)
  private String noIdentitas;
  @Column(name = "email", unique = true, updatable = false, nullable = false)
  private String email;
  @Column(name = "alamat")
  private String alamat;
  @Column(name = "tanggal_lahir", nullable = false)
  private Date tanggalLahir;
  @Column(name = "jenis_kelamin")
  private String jenisKelamin;
  @Column(name = "pekerjaan")
  private String pekerjaan;
  @OneToOne
  @JoinColumn(name = "id_user")
  private User user;
}
