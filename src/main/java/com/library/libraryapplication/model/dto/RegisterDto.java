package com.library.libraryapplication.model.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegisterDto {
  private String username;
  private String password;
  private String namaLengkap;
  private String noIdentitas;
  private String email;
  private String alamat;
  private Date tanggalLahir;
  private String jenisKelamin;
  private String pekerjaan;
}
