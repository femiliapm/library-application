package com.library.libraryapplication.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.library.libraryapplication.assembler.RegisterAssembler;
import com.library.libraryapplication.configuration.DefaultResponse;
import com.library.libraryapplication.model.dto.RegisterDto;
import com.library.libraryapplication.model.entity.DetailBiodata;
import com.library.libraryapplication.repository.BiodataRepository;
import com.library.libraryapplication.service.RegisterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
public class RegisterController {
  @Autowired
  private BiodataRepository repository;
  @Autowired
  private RegisterAssembler assembler;
  @Autowired
  private RegisterService service;

  @GetMapping
  public DefaultResponse get() {
    List<DetailBiodata> biodataList = repository.findAll();
    List<RegisterDto> registerDtos = biodataList.stream().map(entity -> assembler.fromEntity(entity)).collect(Collectors.toList());
    return DefaultResponse.ok(registerDtos);
  }

  @PostMapping
  public DefaultResponse post(@RequestBody RegisterDto dto) {
    // DetailBiodata detailBiodata = assembler.fromDto(dto);
    // repository.save(detailBiodata);
    return DefaultResponse.ok(service.insert(dto));
  }
}
