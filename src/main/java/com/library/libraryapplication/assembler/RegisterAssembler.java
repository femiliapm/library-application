package com.library.libraryapplication.assembler;

import com.library.libraryapplication.model.dto.RegisterDto;
import com.library.libraryapplication.model.entity.DetailBiodata;
import com.library.libraryapplication.model.entity.User;
import com.library.libraryapplication.repository.BiodataRepository;
import com.library.libraryapplication.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegisterAssembler implements InterfaceAssembler<DetailBiodata, RegisterDto> {
   @Autowired
   private BiodataRepository biodataRepository;
   @Autowired
   private UserRepository userRepository;

   @Override
   public DetailBiodata fromDto(RegisterDto dto) {
      // TODO Auto-generated method stub
      if (dto == null)
         return null;

      DetailBiodata detailBiodata = new DetailBiodata();
      User user = new User();

      if (dto.getAlamat() != null)
         detailBiodata.setAlamat(dto.getAlamat());
      if (dto.getEmail() != null)
         detailBiodata.setEmail(dto.getEmail());
      if (dto.getJenisKelamin() != null)
         detailBiodata.setJenisKelamin(dto.getJenisKelamin());
      if (dto.getNamaLengkap() != null)
         detailBiodata.setNamaLengkap(dto.getNamaLengkap());
      if (dto.getNoIdentitas() != null)
         detailBiodata.setNoIdentitas(dto.getNoIdentitas());
      if (dto.getPekerjaan() != null)
         detailBiodata.setPekerjaan(dto.getPekerjaan());
      if (dto.getTanggalLahir() != null)
         detailBiodata.setTanggalLahir(dto.getTanggalLahir());
      if (dto.getUsername() != null)
         user.setUsername(dto.getUsername());
      if (dto.getPassword() != null)
         user.setPassword(dto.getPassword());
      userRepository.save(user);
      detailBiodata.setUser(user);

      return detailBiodata;
   }

   @Override
   public RegisterDto fromEntity(DetailBiodata entity) {
      // TODO Auto-generated method stub
      if (entity == null)
         return null;
      return RegisterDto.builder().alamat(entity.getAlamat()).email(entity.getEmail())
            .jenisKelamin(entity.getJenisKelamin()).namaLengkap(entity.getNamaLengkap())
            .noIdentitas(entity.getNoIdentitas()).password(entity.getUser().getPassword())
            .pekerjaan(entity.getPekerjaan()).tanggalLahir(entity.getTanggalLahir())
            .username(entity.getUser().getUsername()).build();
   }

}
